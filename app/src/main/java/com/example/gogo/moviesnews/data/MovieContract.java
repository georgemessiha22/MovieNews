package com.example.gogo.moviesnews.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;


/**
 * Created by greatbean on 21/11/16.
 */

public class MovieContract {

	static final String CONTENT_AUTHORITY = "com.example.gogo.moviesnews";
	static final String PATH_MOVIE = "movie";
	private final static Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
	//public static final String PATH_FAVORITE = "favorite";

	public static final class MovieEntry implements BaseColumns {
		public static final Uri CONTENT_URI =
				BASE_CONTENT_URI.buildUpon().appendPath(PATH_MOVIE).build();
		public static final String CONTENT_ITEM_TYPE =
				ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MOVIE;
		public static final String TABLE_NAME = "movie";
		public static final String COLUMN_MOVIE_ID = "movie_id";
		public static final String COLUMN_TITLE = "title";
		public static final String COLUMN_OVERVIEW = "overview";
		public static final String COLUMN_POSTER_PATH = "poster_path";
		public static final String COLUMN_VOTE_AVERAGE = "vote_average";
		public static final String COLUMN_VOTE_AVERAGE_PAGE = "vote_average_page";
		public static final String COLUMN_POPULARITY = "popularity";
		public static final String COLUMN_POPULARITY_PAGE = "pop_page";
		public static final String COLUMN_ADULT = "adult";
		public static final String COLUMN_DATE = "date";
		public static final String COLUMN_HOME_PAGE = "home_page";
		public static final String COLUMN_STATUS = "status";
		public static final String COLUMN_RUNTIME = "runtime";
		public static final String COLUMN_FAVORITE = "favorite";
		static final String CONTENT_TYPE =
				ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MOVIE;

		static Uri buildMovieUri(long id) {
			return ContentUris.withAppendedId(CONTENT_URI, id);
		}
	}
}

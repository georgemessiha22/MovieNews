package com.example.gogo.moviesnews;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

/**
 * Created by greatbean on 21/11/16.
 */
public final class Utility {


	private Utility() {
		throw new AssertionError();
	}

	public static String getPreferredSort(Context activity) {
		SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(activity);
		return p.getString(activity.getString(R.string.pref_key),
		                   activity.getString(R.string.pref_default_order));
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager cm =
				(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
	}
}

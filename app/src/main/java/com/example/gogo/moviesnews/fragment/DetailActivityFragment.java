package com.example.gogo.moviesnews.fragment;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gogo.moviesnews.R;
import com.example.gogo.moviesnews.Utility;
import com.example.gogo.moviesnews.data.MovieContract;
import com.example.gogo.moviesnews.sync.FetchData;
import com.squareup.picasso.Picasso;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment
		implements LoaderManager.LoaderCallbacks<Cursor> {

	public final static int COLUMN_TITLE = 1;
	public final static int COLUMN_POSTER_PATH = 2;
	public final static int COLUMN_VOTE_AVERAGE = 3;
	public final static int COLUMN_DATE = 4;
	public final static int COLUMN_OVERVIEW = 5;
	public final static int COLUMN_RUNTIME = 6;
	public final static int COLUMN_FAVORITE = 7;
	public static final String DETAIL_ID = "ID";
	private final String LOG_TAG = DetailActivityFragment.class.getSimpleName();
	private final int DETAIL_LOADER = 1;
	private final String[] LIST_COLUMNS =
			{MovieContract.MovieEntry.TABLE_NAME + "." + MovieContract.MovieEntry._ID,
			 MovieContract.MovieEntry.COLUMN_TITLE, MovieContract.MovieEntry.COLUMN_POSTER_PATH,
			 MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE,MovieContract.MovieEntry.COLUMN_DATE,
			 MovieContract.MovieEntry.COLUMN_OVERVIEW, MovieContract.MovieEntry.COLUMN_RUNTIME,
			 MovieContract.MovieEntry.COLUMN_FAVORITE};
	private int movie_id;
	private TextView mOverview;
	private TextView mTitleDetail;
	private ImageView mPoster;
	private RatingBar mRate;
	private ImageView mFavorite;
	private LinearLayout mLinearLayout;
	private TextView mLength;
	private boolean mFavoriteFlag;
	private TextView mDate;
	private LinearLayout mReviewsLayout;
	private String mID;
	private FetchData d;

	public DetailActivityFragment() {
	}

	@Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		getLoaderManager().restartLoader(DETAIL_LOADER, null, this);
		super.onActivityCreated(savedInstanceState);
	}

	@Override public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {

		return super.onOptionsItemSelected(item);
	}

	@Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_detail_fragement, menu);
	}

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                                   Bundle savedInstanceState) {
		final View rootview = inflater.inflate(R.layout.fragment_detail_, container, false);
		Bundle mBundle = getArguments();
		if (mBundle != null) {
			movie_id = Integer.parseInt(mBundle.getString(DETAIL_ID));
		}
			mOverview = (TextView) rootview.findViewById(R.id.overview_detail);
			mTitleDetail = (TextView) rootview.findViewById(R.id.title_detail);
			mPoster = (ImageView) rootview.findViewById(R.id.poster_image_detail);
			mRate = (RatingBar) rootview.findViewById(R.id.ratingBar_detail);
			mFavorite = (ImageView) rootview.findViewById(R.id.favorite_detail);
			mDate = (TextView) rootview.findViewById(R.id.release_date_detail);
		mLinearLayout = (LinearLayout) rootview.findViewById(R.id.videos_list_details);
			mReviewsLayout = (LinearLayout) rootview.findViewById(R.id.reviews_list_details);
			mLength = (TextView) rootview.findViewById(R.id.length_detail);
		mFavorite.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v) {
				if (!mFavoriteFlag) {
					mFavorite.setImageResource(R.drawable.ic_favorite_on);
					ContentValues cv = new ContentValues();
					cv.put(MovieContract.MovieEntry.COLUMN_FAVORITE, true);
					getContext().getContentResolver()
					            .update(MovieContract.MovieEntry.CONTENT_URI, cv,
					                    MovieContract.MovieEntry.COLUMN_MOVIE_ID +
					                    "=" + movie_id, null);
					mFavoriteFlag = true;
				} else if (mFavoriteFlag) {
					mFavorite.setImageResource(R.drawable.ic_favorite_off);
					ContentValues cv = new ContentValues();
					cv.put(MovieContract.MovieEntry.COLUMN_FAVORITE, false);
					getContext().getContentResolver()
					            .update(MovieContract.MovieEntry.CONTENT_URI, cv,
					                    MovieContract.MovieEntry.COLUMN_MOVIE_ID +
					                    "=" + movie_id, null);
					mFavoriteFlag = false;
				}
			}
		});
		updateDetails();
		return rootview;
	}

	private void updateDetails() {
		if (Utility.isNetworkAvailable(this.getContext())) {
			// Run network query
			d = new FetchData(getContext(), this);
			d.execute(FetchData.MOVIE_DETAIL, movie_id + "");
		} else {
			Toast.makeText(getContext(), "Check Internet Connection", Toast.LENGTH_LONG).show();
		}
	}

	public void changeDetails() {
		try {
			getLoaderManager().restartLoader(DETAIL_LOADER, null, this);
		} catch (IllegalStateException e) {

		}
	}

	@Override public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new CursorLoader(getActivity(), MovieContract.MovieEntry.CONTENT_URI, LIST_COLUMNS,
		                        MovieContract.MovieEntry.COLUMN_MOVIE_ID + "=" + movie_id, null,
		                        null);

	}

	@Override public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		data.moveToFirst();
		if (data.isFirst()) {
			mOverview.setText(data.getString(COLUMN_OVERVIEW));
			mTitleDetail.setText(data.getString(COLUMN_TITLE));
			mRate.setRating((float) data.getInt(COLUMN_VOTE_AVERAGE));
			mDate.setText(data.getString(COLUMN_DATE).split("-")[0]);
			setFavorite(data.getInt(COLUMN_FAVORITE) != 0);
			int runtime = data.getInt(COLUMN_RUNTIME);
			if (runtime < 2) {
				FetchData d = new FetchData(getContext(), this);
				d.execute(FetchData.MOVIE_DETAIL, movie_id + "","runtime");
			}
			mLength.setText(runtime + " min");
			Picasso.with(getContext())
			       .load(FetchData.POSTER_PATH + data.getString(COLUMN_POSTER_PATH))
			       .error(R.drawable.no).placeholder(R.drawable.loader).into(mPoster);
		}
		data.close();
	}


	@Override public void onLoaderReset(Loader<Cursor> loader) {
	}
	public void setFavorite(boolean favorite) {
		this.mFavoriteFlag = favorite;
		if (!mFavoriteFlag) {
			mFavorite.setImageResource(R.drawable.ic_favorite_off);
		} else if (mFavoriteFlag) {
			mFavorite.setImageResource(R.drawable.ic_favorite_on);
		}
	}

	public void pushVideos(final String[] videolinks) {
		if (videolinks != null && getActivity() != null) {
			mLinearLayout = null;
			mLinearLayout = (LinearLayout) getActivity().findViewById(R.id.videos_list_details);
			mLinearLayout.removeAllViews();
			TextView trailers_headline = new TextView(getContext());
			trailers_headline.setLayoutParams(
					new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
					                           ViewGroup.LayoutParams.WRAP_CONTENT));
			trailers_headline.setText("Trailers");
			trailers_headline.setTextSize(30);
			mLinearLayout.addView(trailers_headline);
			final String youtube_img = "https://img.youtube.com/vi/";
			final String youtube_link = "https://www.youtube.com/watch?v=";
			if (videolinks != null) {
				for (int i = 0; i < videolinks.length; i++) {
					if (videolinks[i] != null) {
						String img_link = youtube_img + videolinks[i] + "/0.jpg";
						String video_link = youtube_link + videolinks[i];
						ImageView v = new ImageView(getContext());
						Picasso.with(getContext()).load(img_link).placeholder(R.drawable.loader)
						       .into(v);
						v.setLayoutParams(new ViewGroup.LayoutParams(mLinearLayout.getWidth(),
						                                             LinearLayout.LayoutParams
								                                             .WRAP_CONTENT));
						v.setContentDescription(video_link);
						v.setAdjustViewBounds(true);
						v.setOnClickListener(new View.OnClickListener() {
							@Override public void onClick(View v) {
								String link = v.getContentDescription().toString();
								Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
								startActivity(webIntent);
							}
						});
						mLinearLayout.addView(v);
					}
				}
				mLinearLayout.refreshDrawableState();
			}
		}
	}

	@Override public void onDestroy() {
		if (d != null) {
			d.fragment = null;
		}
		super.onDestroy();

	}

	public void pushReviews(String[] res) {
		if (res != null && getActivity() != null) {
			mReviewsLayout = (LinearLayout) getActivity().findViewById(R.id.reviews_list_details);
			mReviewsLayout.removeAllViews();
			TextView reviews_headline = new TextView(getContext());
			reviews_headline.setLayoutParams(
					new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
					                           ViewGroup.LayoutParams.WRAP_CONTENT));
			reviews_headline.setText("Reviews");
			reviews_headline.setTextSize(30);
			mReviewsLayout.addView(reviews_headline);
			for (int i = 0; i < res.length; i++) {
				if (res[i] != null) {
					TextView v = new TextView(getContext());
					v.setText(res[i]+"\n");
					v.setLayoutParams(
							new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
							                           ViewGroup.LayoutParams.MATCH_PARENT));
					v.refreshDrawableState();
					mReviewsLayout.addView(v);
				}
			}
			mReviewsLayout.refreshDrawableState();
		}
	}
}


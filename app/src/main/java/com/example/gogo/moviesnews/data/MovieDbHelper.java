package com.example.gogo.moviesnews.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class MovieDbHelper extends SQLiteOpenHelper {
	private final static String DATABASE_NAME = "movies.db";
	private static final int DATABASE_VERSION = 1;

	MovieDbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override public void onCreate(SQLiteDatabase db) {
		//create Movies DataBase
		/*
		TABLE_NAME
		_ID INTEGER PRIMARY KEY
		COLUMN_MOVIE_ID INTEGER NOT NULL,
		COLUMN_TITLE TEXT NOT NULL,
		COLUMN_OVERVIEW TEXT NOT NULL,
		COLUMN_POSTER_PATH TEXT,
		COLUMN_VOTE_AVERAGE REAL NOT NULL,
		COLUMN_POPULARITY REAL NOT NULL,
		COLUMN_ADULT BOOLEAN,
		COLUMN_DATE INTEGER NOT NULL,
		COLUMN_HOME_PAGE TEXT,
		COLUMN_STATUS TEXT,
		COLUMN_RUNTIME INTEGER);
		 */
		final String SQL_CREATE_MOVIE_TABLE =
				"CREATE TABLE " + MovieContract.MovieEntry.TABLE_NAME + " (" +
				MovieContract.MovieEntry._ID + " INTEGER PRIMARY " +
				"KEY, " +
				MovieContract.MovieEntry.COLUMN_MOVIE_ID + " INTEGER " +
				"NOT NULL UNIQUE, " +
				MovieContract.MovieEntry.COLUMN_TITLE + " TEXT NOT " +
				"NULL, " +

				MovieContract.MovieEntry.COLUMN_OVERVIEW + " TEXT NOT" +
				" NULL, " +
				MovieContract.MovieEntry.COLUMN_POSTER_PATH + " TEXT," +
				" " +
				MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE_PAGE + " INTEGER, " +
				MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE + " REAL" +
				" NOT NULL, " +
				MovieContract.MovieEntry.COLUMN_POPULARITY_PAGE + " INTEGER,  " +
				MovieContract.MovieEntry.COLUMN_POPULARITY + " REAL " +
				"NOT NULL, " +
				MovieContract.MovieEntry.COLUMN_ADULT + " BOOLEAN, " +
				MovieContract.MovieEntry.COLUMN_DATE + " TEXT NOT " +
				"NULL, " +
				MovieContract.MovieEntry.COLUMN_HOME_PAGE + " TEXT, " +
				MovieContract.MovieEntry.COLUMN_STATUS + " TEXT, " +
				MovieContract.MovieEntry.COLUMN_RUNTIME + " INTEGER, " +
				MovieContract.MovieEntry.COLUMN_FAVORITE + " BOOLEAN" + ");";

		db.execSQL(SQL_CREATE_MOVIE_TABLE);
	}

	@Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + MovieContract.MovieEntry.TABLE_NAME);
		onCreate(db);
	}

	@Override public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//super.onDowngrade(db, oldVersion, newVersion);
		db.execSQL("DROP TABLE IF EXISTS " + MovieContract.MovieEntry.TABLE_NAME);
		onCreate(db);
	}

}

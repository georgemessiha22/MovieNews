package com.example.gogo.moviesnews.fragment;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.example.gogo.moviesnews.R;

/**
 * Created by greatbean on 21/11/16.
 */
public class SettingsFragment extends PreferenceFragment implements Preference
		                                                              .OnPreferenceChangeListener {


	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
	}

	@Override public boolean onPreferenceChange(Preference preference, Object newValue) {
		String stringValue = newValue.toString();
		if (preference instanceof ListPreference) {
			ListPreference lp = (ListPreference) preference;
			int prefIndex = lp.findIndexOfValue(stringValue);
			if (prefIndex >= 0) {
				preference.setSummary(lp.getEntries()[prefIndex]);
			}
		} else {
			preference.setSummary(stringValue);
		}
		return true;
	}
}

package com.example.gogo.moviesnews;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.gogo.moviesnews.fragment.DetailActivityFragment;


public class DetailActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Bundle args = new Bundle();
        args.putString(DetailActivityFragment.DETAIL_ID,
                       getIntent().getStringExtra(DetailActivityFragment.DETAIL_ID));
        DetailActivityFragment n = new DetailActivityFragment();
        n.setArguments(args);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_detail, n).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

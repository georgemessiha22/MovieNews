package com.example.gogo.moviesnews.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.gogo.moviesnews.MainActivity;
import com.example.gogo.moviesnews.R;
import com.example.gogo.moviesnews.data.MovieProvider;
import com.example.gogo.moviesnews.fragment.MovieListFragment;
import com.example.gogo.moviesnews.sync.FetchData;
import com.squareup.picasso.Picasso;


/**
 * Created by greatbean on 21/11/16.
 */
public class MovieAdapter extends CursorRecyclerViewAdapter<MovieAdapter.ViewHolder> {

	private static final String LOG_TAG = MovieProvider.class.getSimpleName();
	private Context mContext;


	public MovieAdapter(Context context, Cursor cursor) {
		super(context, cursor);
		mContext = context;

	}


	@Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext())
		                              .inflate(R.layout.movie_item, parent, false);
		ViewHolder vh = new ViewHolder(itemView);

		return vh;
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor, final int position) {
		final MovieItem movie = MovieItem.fromCursor(cursor);
		if (MainActivity.mTwoPane) {
			if (position == 0) {
				if (MovieListFragment.mPosition == ListView.INVALID_POSITION) {
					MovieListFragment.mPosition = position;
					((MovieListFragment.CallBack) mContext).onItemSelected(movie.getMovie_id());
				}
			}
		}
		Picasso.with(mContext).load(FetchData.POSTER_PATH + movie.getPath())
		       .error(R.drawable.no)
		       .placeholder(R.drawable.loader)
		       .into(viewHolder.mImageView);
		viewHolder.position = position;
		viewHolder.mImageView.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v) {
				((MovieListFragment.CallBack) mContext).onItemSelected(movie.getMovie_id());
				MovieListFragment.mPosition = position;
			}
		});
		viewHolder.title.setText(movie.getTitle());
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public int position;
		ImageView mImageView;
		TextView title;

		public ViewHolder(View view) {
			super(view);
			mImageView = (ImageView) view.findViewById(R.id.movie_poster);
			title = (TextView) view.findViewById(R.id.movie_item_title);
		}

	}
}


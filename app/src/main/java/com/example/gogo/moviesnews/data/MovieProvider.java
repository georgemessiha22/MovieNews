package com.example.gogo.moviesnews.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;


public class MovieProvider extends ContentProvider {

	static final int MOVIE = 100;
	private static final UriMatcher sUriMatcher = buildUriMatcher();
	private static final String LOG_TAG = MovieProvider.class.getSimpleName();
	private MovieDbHelper mOpenHelper;

	private static UriMatcher buildUriMatcher() {
		final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
		final String authority = MovieContract.CONTENT_AUTHORITY;
		matcher.addURI(authority, MovieContract.PATH_MOVIE, MOVIE);
		return matcher;
	}

	@Override public boolean onCreate() {
		mOpenHelper = new MovieDbHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
	                    String sortOrder) {
		Cursor reCursor;
		switch (sUriMatcher.match(uri)) {
			case MOVIE: {
				reCursor = mOpenHelper.getReadableDatabase()
				                      .query(MovieContract.MovieEntry.TABLE_NAME, projection,
				                             selection, selectionArgs, null, null, sortOrder);
				break;
			}
			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
		reCursor.setNotificationUri(getContext().getContentResolver(),uri);
		return reCursor;
	}

	@Override public String getType(Uri uri) {
		final int match = sUriMatcher.match(uri);
		switch (match) {
			case MOVIE:
				return MovieContract.MovieEntry.CONTENT_TYPE;
			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
	}

	@Override public Uri insert(Uri uri, ContentValues values) {
		final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		final int match = sUriMatcher.match(uri);
		Uri returnUri;
		switch (match) {
			case MOVIE:{
				long _id = db.insert(MovieContract.MovieEntry.TABLE_NAME,null,values);
				if(_id >0){
					returnUri = MovieContract.MovieEntry.buildMovieUri(_id);
				} else {
					throw new android.database.SQLException("Failed To insert "+ uri);
				}
				break;
			}
			default :
				throw new UnsupportedOperationException("Unkown uri: "+uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);

		return returnUri;
	}

	@Override public int delete(Uri uri, String selection, String[] selectionArgs) {
		final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		final int match = sUriMatcher.match(uri);
		int rowsDeleted;
		// this makes delete all rows return the number of rows deleted
		if (null == selection)
			selection = "1";
		switch (match) {
			case MOVIE:
				rowsDeleted = db.delete(MovieContract.MovieEntry.TABLE_NAME, selection,
				                        selectionArgs);
				break;

			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
		// Because a null deletes all rows
		if (rowsDeleted != 0) {
			getContext().getContentResolver().notifyChange(uri, null);
		}
		return rowsDeleted;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		final int match = sUriMatcher.match(uri);
		int rowsUpdated;
		switch (match) {
			case MOVIE:
				rowsUpdated = db.update(MovieContract.MovieEntry.TABLE_NAME, values, selection,
				                        selectionArgs);
				Log.v(LOG_TAG," Rows updated: "+rowsUpdated);
				break;
			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
		if (rowsUpdated != 0) {
			getContext().getContentResolver().notifyChange(uri, null);
		}
		return rowsUpdated;
	}

	@Override public int bulkInsert(Uri uri, ContentValues[] values) {
		final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		final int match = sUriMatcher.match(uri);
		switch (match) {
			case MOVIE:
				db.beginTransaction();
				int returnCount = 0;
				try {
					for (ContentValues value : values) {
						try {
							long _id = db.insertOrThrow(MovieContract.MovieEntry.TABLE_NAME, null,
							                            value);
							if (_id != -1) {
								returnCount++;
							}
						} catch (SQLException e) {
							try {
								String selection =
										MovieContract.MovieEntry.COLUMN_MOVIE_ID + " = " +
										"" + value.get(MovieContract.MovieEntry.COLUMN_MOVIE_ID);
								value.remove(MovieContract.MovieEntry.COLUMN_MOVIE_ID);
								int update = db.update(MovieContract.MovieEntry.TABLE_NAME, value,
								                       selection, null);
								if (update != -1) {
									returnCount++;
								}
							} catch (SQLException e1) {
								Log.v(LOG_TAG, e.getMessage());
							}
						}
					}
					db.setTransactionSuccessful();
				}  finally {

					db.endTransaction();
				}
				getContext().getContentResolver().notifyChange(uri, null);
				return returnCount;
			default:
				return super.bulkInsert(uri, values);
		}
	}
}

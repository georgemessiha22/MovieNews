package com.example.gogo.moviesnews;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.gogo.moviesnews.fragment.DetailActivityFragment;
import com.example.gogo.moviesnews.fragment.MovieListFragment;

public class MainActivity extends AppCompatActivity implements MovieListFragment.CallBack {

	private static final String LOG_TAG = MainActivity.class.getSimpleName();
	private static final String DETAILFRAGMENT_TAG = "DFTAG";
	public static boolean mTwoPane;
	private String mSort;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mSort = Utility.getPreferredSort(this);
		if (findViewById(R.id.fragment_detail) != null) {
			mTwoPane = true;
			if (savedInstanceState == null) {
				getSupportFragmentManager().beginTransaction().replace(R.id.fragment_detail,
				                                                       new
						                                                       DetailActivityFragment(),

				                                                       DETAILFRAGMENT_TAG)

				                           .commit();
			}
		} else {
			mTwoPane = false;
		}
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override protected void onResume() {
		super.onResume();
		String sort = Utility.getPreferredSort(this);
		Log.v(LOG_TAG, "sort by: " + sort);
		if (sort != null && !mSort.equals(sort)) {
			MovieListFragment mm = (MovieListFragment) getSupportFragmentManager()
					                                           .findFragmentById(
					R.id.main_fragment);
			if (null != mm) {
				mm.changeLoader();
			}
			mSort = sort;
		}
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override public void onItemSelected(String movieId) {
		if (mTwoPane) {
			Bundle args = new Bundle();
			args.putString(DetailActivityFragment.DETAIL_ID, movieId);
			DetailActivityFragment df = new DetailActivityFragment();
			df.setArguments(args);
			getSupportFragmentManager().beginTransaction()
			                           .replace(R.id.fragment_detail, df, DETAILFRAGMENT_TAG)
			                           .commit();
		} else {
			Intent n = new Intent(this, DetailActivity.class);
			n.putExtra(DetailActivityFragment.DETAIL_ID, movieId);
			startActivity(n);
		}
	}
}

package com.example.gogo.moviesnews.sync;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.example.gogo.moviesnews.BuildConfig;
import com.example.gogo.moviesnews.R;
import com.example.gogo.moviesnews.data.MovieContract;
import com.example.gogo.moviesnews.fragment.DetailActivityFragment;
import com.example.gogo.moviesnews.fragment.MovieListFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Vector;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by greatbean on 13/11/16.
 */
public class FetchData extends AsyncTask<String, Void, Void> {
	public static final String POSTER_PATH = "https://image.tmdb.org/t/p/w500";
	public static final String MOVIE_LIST = "movie_list";
	public static final String MOVIE_DETAIL = "movie_detail";
	private static final String LOG_TAG = FetchData.class.getSimpleName();
	private final String MOVIEDB_URL = "https://api.themoviedb.org/3/movie/";
	private final String MOVIEDB_POPULAR_URL = "popular?";
	private final String MOVIEDB_TOP_RATED_URL = "top_rated?";
	private final String MOVIEDB_REVIEWS = "/reviews?";
	private final String MOVIEDB_TRAILERS = "/videos?";
	public Fragment fragment;
	private Context mContext;
	private String[] res;
	private String[] videolinks;
	private boolean detailFlag;

	public FetchData(Context c, Fragment movieListFragment) {
		mContext = c;
		fragment = movieListFragment;
	}


	String run(String url) throws IOException {
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder().url(url).build();

		Response response = client.newCall(request).execute();
		return response.body().string();
	}

	/**
	 * @param params String of sortedBy,
	 */
	@Override protected Void doInBackground(String... params) {

		final String API_KEY = "api_key";
		final String PARAM_PAGE_NUMBER = "page";
		if (params[0] == null) {
			return null;
		} else if (params.length > 1 &&
		           !params[1].equals(mContext.getString(R.string.pref_order_favorite))) {
			if (params[0].equals(MOVIE_LIST)) {
				String uri = MOVIEDB_URL;
				String page = "1";
				String sort = params[1];
				if (params[1].equals(mContext.getString(R.string.pref_order_popularity))) {
					uri += MOVIEDB_POPULAR_URL;
				} else if (params[1].equals(mContext.getString(R.string.pref_order_rate))) {
					uri += MOVIEDB_TOP_RATED_URL;
				}
				if (params.length > 2) {
					if (params[2] != null) {
						page = "" + Integer.parseInt(params[2]);
					}
				}
				Uri builtUri = Uri.parse(uri).buildUpon()
				                  .appendQueryParameter(API_KEY, BuildConfig.OPEN_MOVIE_DB_API_KEY)
				                  .appendQueryParameter(PARAM_PAGE_NUMBER, page).build();
				try {
					getMovieDataFromJson(run(builtUri.toString()), page, sort);
				} catch (JSONException e) {
					Log.e(LOG_TAG, e.getMessage(), e);
				} catch (IOException e) {
					Log.e(LOG_TAG, e.getMessage(), e);
				}
			} else if (params[0].equals(MOVIE_DETAIL)) {
				try {
					String uri = MOVIEDB_URL + params[1];
					Uri builtUri = Uri.parse(uri).buildUpon().appendQueryParameter(API_KEY,
					                                                               BuildConfig
							                                                               .OPEN_MOVIE_DB_API_KEY)


					                  .build();
					getMovieDetailFromJson(run(builtUri.toString()), params[1]);
					// getVideosList
					builtUri = Uri.parse(MOVIEDB_URL + params[1] + MOVIEDB_TRAILERS).buildUpon()
					              .appendQueryParameter(API_KEY, BuildConfig.OPEN_MOVIE_DB_API_KEY)
					              .build();
					getVideoLinks(run(builtUri.toString()));
					// getReviewsList
					builtUri = Uri.parse(MOVIEDB_URL + params[1] + MOVIEDB_REVIEWS).buildUpon()
					              .appendQueryParameter(API_KEY, BuildConfig.OPEN_MOVIE_DB_API_KEY)
					              .build();
					getReviewsLinks(run(builtUri.toString()));
				} catch (JSONException e) {
					Log.v(LOG_TAG, e.getMessage(), e);
				} catch (IOException e) {
					Log.v(LOG_TAG, e.getMessage(), e);
				}
			}
		}
		return null;
	}

	private void getReviewsLinks(String reviewsJsonStr) throws JSONException {
		JSONObject js = new JSONObject(reviewsJsonStr);
		JSONArray arr = js.getJSONArray("results");
		res = new String[arr.length()];
		final String OWM_AUTHOR = "author";
		final String OWM_CONTENT = "content";
		final String OWM_URL = "url";
		final String OWM_ID = "id";
		//id string optional author string optional content string optional url string
		for (int i = 0; i < arr.length(); i++) {
			JSONObject review = (JSONObject) arr.get(i);
			res[i] = review.getString(OWM_AUTHOR) + ": " + review.getString(OWM_CONTENT);
		}
	}

	private void getVideoLinks(String videosJsonStr) throws JSONException {
		JSONObject js = new JSONObject(videosJsonStr);
		JSONArray videos = js.getJSONArray("results");
		videolinks = new String[videos.length()];

		for (int i = 0; i < videos.length(); i++) {
			JSONObject link = (JSONObject) videos.get(i);
			if (link.getString("site").equalsIgnoreCase("youtube")) {
				videolinks[i] = link.getString("key");
			}
		}
	}

	private void getMovieDataFromJson(String movieJsonStr, String page, String sort)
			throws JSONException {
		/**
		 * poster_path:/xfWac8MTYDxujaxgPVcRD9yZaul.jpg
		 * adult:false
		 * overview:After his career is destroyed, a brilliant but arrogant surgeon gets a new
		 * lease on life when a sorcerer takes him under his wing and trains him to defend
		 * the world against evil.
		 * release_date:2016-10-25
		 * genre_ids4 Items
		 * id:284052
		 * original_title:Doctor Strange
		 * original_language:en
		 * title:Doctor Strange
		 * backdrop_path:/tFI8VLMgSTTU38i8TIsklfqS9Nl.jpg
		 * popularity:52.429714
		 * vote_count:723
		 * video:false
		 * vote_average:6.93
		 */
		final String MOVIES_LIST = "results";
		final String OWM_POSTER_PATH = "poster_path";
		final String OWM_ADULT = "adult";
		final String OWM_OVERVIEW = "overview";
		final String OWM_RELEASE_DATE = "release_date";
		final String OWM_ID = "id";
		final String OWM_TITLE = "title";
		final String OWM_POPULARITY = "popularity";
		final String OWM_VOTE_AVERAGE = "vote_average";
		final int page_no = Integer.parseInt(page) * 100;
		try {
			JSONObject jsonObj = new JSONObject(movieJsonStr);
			JSONArray results = jsonObj.getJSONArray(MOVIES_LIST);
			Vector<ContentValues> contentValuesVector = new Vector<ContentValues>(results.length
					                                                                              ());
			for (int i = 0; i < results.length(); i++) {
				JSONObject movie = results.getJSONObject(i);
				String title = movie.getString(OWM_TITLE);
				int id = movie.getInt(OWM_ID);
				String poster_path = movie.getString(OWM_POSTER_PATH);
				String overview = movie.getString(OWM_OVERVIEW);
				double rate = movie.getDouble(OWM_VOTE_AVERAGE);
				String date = movie.getString(OWM_RELEASE_DATE);
				float pop = movie.getLong(OWM_POPULARITY);
				boolean adult = movie.getBoolean(OWM_ADULT);
				ContentValues movieValues = new ContentValues();
				movieValues.put(MovieContract.MovieEntry.COLUMN_TITLE, title);
				movieValues.put(MovieContract.MovieEntry.COLUMN_MOVIE_ID, id);
				movieValues.put(MovieContract.MovieEntry.COLUMN_POSTER_PATH, poster_path);
				movieValues.put(MovieContract.MovieEntry.COLUMN_OVERVIEW, overview);
				movieValues.put(MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE, rate);
				movieValues.put(MovieContract.MovieEntry.COLUMN_DATE, date);
				movieValues.put(MovieContract.MovieEntry.COLUMN_POPULARITY, pop);
				movieValues.put(MovieContract.MovieEntry.COLUMN_ADULT, adult);
				if (sort.equals(mContext.getString(R.string.pref_order_popularity))) {
					movieValues.put(MovieContract.MovieEntry.COLUMN_POPULARITY_PAGE, (page_no +
					                                                                  i));
				} else if (sort.equals(mContext.getString(R.string.pref_order_rate))) {
					movieValues
							.put(MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE_PAGE, (page_no + i));
				}
				contentValuesVector.add(movieValues);
			}
			int inserted = 0;
			if (contentValuesVector.size() > 0) {
				ContentValues[] cvArray = new ContentValues[contentValuesVector.size()];
				contentValuesVector.toArray(cvArray);
				inserted = mContext.getContentResolver()
				                   .bulkInsert(MovieContract.MovieEntry.CONTENT_URI, cvArray);
			}

			Log.d(LOG_TAG, "FetchWeatherTask Complete. " + inserted + " Inserted");

		} catch (JSONException e) {
			Log.e(LOG_TAG, e.getMessage(), e);
			e.printStackTrace();
		}
	}


	private void getMovieDetailFromJson(String movieDetailJsonStr, String id) throws
	                                                                          JSONException {
		/**
		 *  "adult": false,
		 "backdrop_path": "/tFI8VLMgSTTU38i8TIsklfqS9Nl.jpg",
		 "belongs_to_collection": null,
		 "budget": 165000000,
		 "genres": [{"id": 28,"name": "Action"},{"id": 12,"name": "Adventure"},{"id": 14,"name":
		 "Fantasy"},{"id": 878,"name": "Science Fiction"}],
		 "homepage": "http://marvel.com/doctorstrangepremiere",
		 "id": 284052,
		 "imdb_id": "tt1211837",
		 "original_language": "en",
		 "original_title": "Doctor Strange",
		 "overview": "After his career is destroyed, a brilliant but arrogant surgeon gets a new
		 lease on life when a sorcerer takes him under his wing and trains him to defend the world
		 against evil.",
		 "popularity": 45.682606,
		 "poster_path": "/xfWac8MTYDxujaxgPVcRD9yZaul.jpg",
		 "production_companies": [{"name": "Marvel Studios","id": 420}],
		 "production_countries": [{"iso_3166_1": "US","name": "United States of America"}],
		 "release_date": "2016-10-25",
		 "revenue": 494028124,
		 "runtime": 115,
		 "spoken_languages": [{"iso_639_1": "en","name": "English"}],
		 "status": "Released",
		 "tagline": "Open your mind. Change your reality.",
		 "title": "Doctor Strange",
		 "video": false,
		 "vote_average": 6.7,
		 "vote_count": 1035
		 */

		JSONObject msg = new JSONObject(movieDetailJsonStr);
		int runtime = msg.getInt("runtime");
		ContentValues cv = new ContentValues();
		cv.put(MovieContract.MovieEntry.COLUMN_RUNTIME, runtime);
		int x = mContext.getContentResolver().update(MovieContract.MovieEntry.CONTENT_URI, cv,
		                                             MovieContract.MovieEntry.COLUMN_MOVIE_ID +
		                                             "=" + id, null);
		if (x == 1) {
			detailFlag = true;
		}
	}


	@Override protected void onPostExecute(Void aVoid) {
		super.onPostExecute(aVoid);
		if (fragment != null) {
			if (fragment instanceof MovieListFragment) {
				((MovieListFragment) fragment).changeLoader();
				((MovieListFragment) fragment).loadEnable();
				Log.v(LOG_TAG, "MovieListFragment Change Loader.");
			} else if (fragment instanceof DetailActivityFragment) {
				if (detailFlag) {
					((DetailActivityFragment) fragment).changeDetails();
					detailFlag = false;
				}
				((DetailActivityFragment) fragment).pushReviews(res);
				((DetailActivityFragment) fragment).pushVideos(videolinks);
				videolinks = null;
				res = null;
			}
		}
	}
}

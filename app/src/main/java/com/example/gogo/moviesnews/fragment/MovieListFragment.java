package com.example.gogo.moviesnews.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gogo.moviesnews.MainActivity;
import com.example.gogo.moviesnews.R;
import com.example.gogo.moviesnews.Utility;
import com.example.gogo.moviesnews.adapter.MovieAdapter;
import com.example.gogo.moviesnews.data.MovieContract;
import com.example.gogo.moviesnews.sync.FetchData;

/**
 * A placeholder fragment containing a simple view.
 */
public class MovieListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {


	public static final int COL_MOVIE_DB_ID = 0;
	public static final int COL_MOVIE_TITLE = 1;
	public static final int COL_MOVIE_POSTER_PATH = 2;
	public static final int COL_MOVIE_POPULARITY = 3;
	public static final int COL_MOVIE_VOTE_AVERAGE = 4;
	public static final int COL_MOVIE_ID = 5;
	private static final int MOVIE_LOADER = 0;
	private static final String[] LIST_COLUMNS = {MovieContract.MovieEntry.TABLE_NAME + "" +
	                                              "." + MovieContract.MovieEntry._ID,
	                                              MovieContract.MovieEntry.COLUMN_TITLE,
	                                              MovieContract.MovieEntry.COLUMN_POSTER_PATH,
	                                              MovieContract.MovieEntry.COLUMN_POPULARITY,
	                                              MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE,
	                                              MovieContract.MovieEntry.COLUMN_MOVIE_ID};
	private static final String SELECTED_KEY = "selected";
	public static int mPosition = ListView.INVALID_POSITION;
	public String LOG_TAG = MovieListFragment.class.getSimpleName();
	private MovieAdapter mMovieAdapter;
	private View rootView;
	private boolean loading = true;
	private GridLayoutManager glm;
	private int mPageNumber = 1;
	private RecyclerView mRecyclerView;
	private TextView mEmptyView;


	public MovieListFragment() {
	}

	@Override public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.movielistfragment, menu);
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_refresh) {
			updateMovie();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                                   Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_movie_list, container, false);
		mEmptyView = (TextView) rootView.findViewById(R.id.empty_view);
		mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
		mMovieAdapter = new MovieAdapter(getActivity(), null);
		if (savedInstanceState != null && savedInstanceState.containsKey(SELECTED_KEY)) {
			mPosition = savedInstanceState.getInt(SELECTED_KEY);
		}
		return rootView;
	}

	@Override public void onSaveInstanceState(Bundle outState) {
		if (mPosition != ListView.INVALID_POSITION) {
			outState.putInt(SELECTED_KEY, mPosition);
		}
		super.onSaveInstanceState(outState);
	}

	public void loadEnable() {
		loading = true;
	}

	@Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		mRecyclerView.setAdapter(mMovieAdapter);
		mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				if (dy > 0) //check for scroll down
				{
					int pastVisiblesItems, visibleItemCount, totalItemCount;
					visibleItemCount = glm.getChildCount();
					totalItemCount = glm.getItemCount();
					pastVisiblesItems = glm.findFirstVisibleItemPosition();

					if (loading) {
						if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
							loading = false;
							mPageNumber++;
							updateMovie();
							Log.v(LOG_TAG, " page no: " + mPageNumber);
						}
					}
				}
			}
		});
		getLoaderManager().restartLoader(MOVIE_LOADER, null, this);
		if (MainActivity.mTwoPane) {
			glm = new GridLayoutManager(getContext(), 2);
		} else {
			glm = new GridLayoutManager(getContext(), 3);
		} mRecyclerView.setLayoutManager(glm);
		super.onActivityCreated(savedInstanceState);
		updateMovie();
	}

	@Override public void onDestroy() {
		super.onDestroy();
	}

	public void updateMovie() {
		FetchData f = new FetchData(getActivity(), this);
		String sort = Utility.getPreferredSort(getActivity());
		if (Utility.isNetworkAvailable(this.getContext())) {
			// Run network query
			f.execute(FetchData.MOVIE_LIST, sort, mPageNumber + "");
		} else {
			Toast.makeText(getContext(), "Check Internet Connection", Toast.LENGTH_LONG).show();
		}
	}

	public void changeLoader() {
		getLoaderManager().restartLoader(MOVIE_LOADER, null, this);
	}

	@Override public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		String order = Utility.getPreferredSort(getContext());
		String selection = null;
		String sortBy = null;
		if (order.equals(getString(R.string.pref_order_popularity))) {
			sortBy = MovieContract.MovieEntry.COLUMN_POPULARITY + " DESC, " +
			         MovieContract.MovieEntry.COLUMN_POPULARITY_PAGE + " ASC";
		} else if (order.equals(getString(R.string.pref_order_rate))) {
			sortBy = MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE + " DESC, " +
			         MovieContract.MovieEntry.COLUMN_VOTE_AVERAGE_PAGE + " ASC";
		} else if (order.equals(getString(R.string.pref_order_favorite))) {
			selection = MovieContract.MovieEntry.COLUMN_FAVORITE + " = 1";
		}
		return new CursorLoader(getActivity(), MovieContract.MovieEntry.CONTENT_URI, LIST_COLUMNS,
		                        selection, null, sortBy);
	}


	@Override public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mMovieAdapter.changeCursor(data);
		if (mPosition != ListView.INVALID_POSITION) {
			// If we don't need to restart the loader, and there's a desired position to restore
			// to, do so now.
			mRecyclerView.smoothScrollToPosition(mPosition);
		}
		if (data.getCount() < 1) {
			mRecyclerView.setVisibility(View.GONE);
			mEmptyView.setVisibility(View.VISIBLE);
		} else {
			mRecyclerView.setVisibility(View.VISIBLE);
			mEmptyView.setVisibility(View.GONE);
		}
	}

	@Override public void onLoaderReset(Loader<Cursor> loader) {
		mMovieAdapter.swapCursor(null);
	}

	public interface CallBack {
		void onItemSelected(String movieId);
	}
}
package com.example.gogo.moviesnews;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.example.gogo.moviesnews.fragment.SettingsFragment;

public class SettingsActivity extends AppCompatActivity {


	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		if(savedInstanceState==null){
			getFragmentManager().beginTransaction()
			                    .replace(R.id.fragment_settings, new SettingsFragment()).commit();
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}
}


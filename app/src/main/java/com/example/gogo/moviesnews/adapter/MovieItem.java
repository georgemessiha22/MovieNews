package com.example.gogo.moviesnews.adapter;

import android.database.Cursor;

import com.example.gogo.moviesnews.fragment.MovieListFragment;

public class MovieItem {
	private String movie_id;
	private String path;
	private String title;

	public static MovieItem fromCursor(Cursor cursor) {
		//TODO return your MyListItem from cursor.
		MovieItem mi = new MovieItem();
		mi.setMovie_id(cursor.getString(MovieListFragment.COL_MOVIE_ID));
		mi.setPath(cursor.getString(MovieListFragment.COL_MOVIE_POSTER_PATH));
		mi.setTitle(cursor.getString(MovieListFragment.COL_MOVIE_TITLE));
		return mi;
	}

	public String getMovie_id() {
		return movie_id;
	}

	public void setMovie_id(String movie_id) {
		this.movie_id = movie_id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
